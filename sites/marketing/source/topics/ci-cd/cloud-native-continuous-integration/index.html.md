---
layout: markdown_page
title: "What is cloud native continuous integration?"
description: "Cloud native development needs continuous integration that supports speed. See what separates cloud native CI from regular CI."
---

In modern software development, most teams are already practicing continuous integration (CI). As DevOps teams look to increase velocity and scale, they look to cloud computing to help them achieve those goals. This kind of development is called cloud native development. These two concepts, CI and cloud native, work together so that teams can deploy to different environments. 


## Cloud native + continuous integration

[Cloud native](/cloud-native/){:target="_blank"} is a way to build and run applications that take advantage of the scalability of the cloud computing model. Cloud native computing uses modern cloud services, like container orchestration, serverless, and [multicloud](/topics/multicloud/){:target="_blank"} to name a few. Cloud native applications are built to run in the cloud.

CI is the practice of integrating code into a shared repository and building/testing each change automatically, several times per day. For teams using [pipeline as code](/topics/ci-cd/pipeline-as-code/){:target="_blank"}, they can configure builds, tests, and deployment in code that is trackable and stored in the same shared repository as their source code.

Cloud native continuous integration is simply continuous integration that can supports cloud services often used in cloud native development.


## What a cloud native CI pipeline needs

Cloud native offers opportunities in terms of velocity and scale, but also [increases complexity](https://thenewstack.io/the-shifting-nature-of-ci-cd-in-the-age-of-cloud-native-computing/){:target="_blank"}. Cloud native engineering teams need increased automation and stability, and CI/CD tools designed to support the complexity that comes from developing in a [microservices](/topics/microservices/){:target="_blank"} environment. 

For better cloud native development, teams should ensure their continuous integration solutions are optimized for the cloud services they commonly use:

- Container orchestration tools, like [Kubernetes](/solutions/kubernetes/){:target="_blank"}, allow developers to coordinate the way in which an application’s containers will function, including scaling and deployment. For teams using Kubernetes, their cloud native CI should have a robust Kubernetes integration to support adding and/or managing multiple clusters. 

- Seamless [continuous delivery](/stages-devops-lifecycle/continuous-delivery/){:target="_blank"} (CD), in addition to continuous integration, is important for cloud native and microservices development. High-functioning deployment strategies, like [canary deployments](https://docs.gitlab.com/ee/user/project/canary_deployments.html){:target="_blank"}, can help cloud native teams test new features with the same velocity they use to build them.

- Cloud native applications are often architectured using microservices instead of a monolithic application structure, and rely on containers to package the application’s libraries and processes for deployment. A cloud native CI tool with [built-in container registry](https://docs.gitlab.com/ee/user/packages/container_registry/index.html){:target="_blank"} can help streamline this process.

Cloud native continuous integration is designed to support the cloud services and architectures cloud native teams use, and offers the automation teams need for speed and stability. 


## More about cloud native development

[Deploy software from GitLab CI/CD pipelines to Kubernetes](https://docs.gitlab.com/ee/user/project/clusters/){:target="_blank"}

[How to deploy on AWS from GitLab](/resources/whitepaper-deploy-aws-gitlab/){:target="_blank"}

[How to create a CI/CD pipeline with Auto Deploy to Kubernetes using GitLab and Helm](/blog/2017/09/21/how-to-create-ci-cd-pipeline-with-autodeploy-to-kubernetes-using-gitlab-and-helm/){:target="_blank"}
