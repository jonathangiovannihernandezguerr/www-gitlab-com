---
layout: handbook-page-toc
title: "Product Design Pairs - FY22-Q1"
description: "Product designer pairs rotation schedule"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

[//]: # TIP: Create the schedule in a temporary spreadsheet, and then copy/paste the rows into an online markdown generator (https://www.google.com/search?q=copy-table-in-excel-and-paste-as-a-markdown-table)

| Product Designer   | Time Zone | Stage            | Section         | Design Pair               |
|--------------------|-----------|------------------|-----------------|---------------------------|
| A Bauerly          | GMT-8     | Monitor          | Ops             |     T Davis               |
| A Ginsberg         | GMT-6     | Plan             | Dev             | J Elder                   |
| A Gray             | GMT-6     | Secure           | Secure & Defend | A Regnery                 |
| A Hughes           | GMT-6     | Manage           | Dev             | P Moreira da Silva        |
| A Regnery          | GMT-5     | Manage           | Dev             | A Gray                    |
| A Volpe            | GMT-5     | Secure           | Secure & Defend | M Nichols                 |
| B Lippert          | GMT-6     | Secure           | Secure & Defend | T Noah                    |
| C Yang             | GMT+1     | Secure           | Secure & Defend | D Mora                    |
| D Fosco            | GMT+1     | Release          | CI/CD           | M Latin                   |
| D Mora             | GMT+1     | Manage           | Dev             | C Yang                    |
| E Bauman           | GMT-5     | Acquisition      | Growth          | N Sotnikova               |
| E Sybrant          | GMT-6     | Acquisition      | Growth          | N Brandt                  |
| H Reynolds         | GMT-5     | Plan             | Dev             | M Vrachni                 |
| I Camacho          | GMT+1     | Package          | CI/CD           | L Vanc                    |
| J Elder            | GMT-6     | Foundations      | Dev             | A Ginsberg                |
| J Ostrowski        | GMT-5     | Foundations      | Dev             | K Comoli                  |
| K Comoli           | GMT+1     | Conversion       | Growth          | J Ostrowski               |
| L Vanc             | GMT-8     | Ecosystem        | Enablement      | A Hughes                  |
| M Latin            | GMT+2     | Expansion        | Growth          | D Fosco                   |
| M Le               | GMT+11    | Create           | Dev             | V Mishra                  |
| M Nearents         | GMT-8     | Utilization      | Fulfillment     | S Park                    |
| M Nichols          | GMT-5     | Release          | CI/CD           | A Volpe                   |
| M Vrachni          | GMT+0     | Configure        | Ops             | H Reynolds                |
| N Brandt           | GMT-7     | Plan             | Dev             | E Sybrant                 |
| N Post             | GMT+0     | Manage           | Dev             | R Verissimo               |
| N Sotnikova        | GMT+1     | Monitor          | Ops             | E Bauman                  |
| P Moreira da Silva | GMT+1     | Create           | Dev             | A Hughes                  |
| R Verissimo        | GMT+1     | Verify           | CI/CD           | N Post                    |
| S Park             | GMT+1     | Geo              | Enablement      | M Nearents                |
| T Noah             | GMT+0     | Retention        | Growth          | B Lippert                 |
| V Mishra           | GMT+5.5   | Verify           | CI/CD           | M Le                      |
