require 'spec_helper'
require_relative '../../../lib/team/yaml'

describe Gitlab::Homepage::Team::Yaml do
  let(:base_dir) do
    path = Pathname.new(__dir__) + '../../fixtures/data'
    path.expand_path
  end

  subject { described_class.new(base_dir) }

  it 'can split the team.yml file' do
    files = %w[
      officiis_nemo.yml
      omnis.possimus.yml
      qui_aut.yml
    ]

    files.each do |file|
      path = base_dir / 'team_members' / 'person' / file[0] / file
      person = YAML.load_file(path)
      expect(subject).to receive(:to_file).with(path, person)
    end

    subject.split
  end

  it 'can join a directory of files' do
    fixture = Pathname.new(base_dir / 'team.yml')
    team = YAML.load_file(fixture)

    expect(subject).to receive(:to_file).with(fixture, match_array(team), described_class::HEADER)

    subject.join
  end

  describe '#build_team' do
    context 'the index directories are not all correct' do
      let(:base_dir) do
        path = Pathname.new(__dir__) + '../../fixtures/wrong-index-dir'
        path.expand_path
      end

      it 'raises a WrongPlaceError' do
        expect { subject.send(:build_team) }
          .to raise_error(Gitlab::Homepage::Team::WrongPlaceError)
      end
    end
  end

  describe '#verify!' do
    it 'does not raise' do
      expect { subject.verify! }.not_to raise_error
    end

    context 'the file are inconsistent' do
      let(:base_dir) do
        path = Pathname.new(__dir__) + '../../fixtures/inconsistent-team'
        path.expand_path
      end

      it 'raises an InconsistentTeamError' do
        expect { subject.verify! }
          .to raise_error(Gitlab::Homepage::Team::InconsistentTeamError)
      end
    end

    context 'there is a bad slug' do
      shared_examples 'bad slug team' do
        ['Bad', 'also bad', 'x'].each do |slug|
          context "The slug is #{slug}" do
            let(:bad_slug_team) do
              [{ 'slug' => slug }]
            end

            it 'raises a BadSlug error' do
              expect { subject.verify! }
                .to raise_error(Gitlab::Homepage::Team::BadSlug)
            end
          end
        end
      end

      context 'in the combined file' do
        before do
          allow(subject).to receive(:load_team).and_return(bad_slug_team)
        end

        it_behaves_like 'bad slug team'
      end

      context 'in the split files' do
        before do
          allow(subject).to receive(:build_team).and_return(bad_slug_team)
        end

        it_behaves_like 'bad slug team'
      end
    end
  end
end
